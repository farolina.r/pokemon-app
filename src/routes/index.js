import PageContainer from "../page";

const RoutesList = [
    {
        path: "/",
        component: PageContainer,
        exact: true,
        default: true
    },
];

export default RoutesList;
