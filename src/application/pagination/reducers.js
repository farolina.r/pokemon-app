import { combineReducers } from "redux";
import { PAGE } from "./pages";
import * as types from "./types";

function paginationReducer(state = {
    page: PAGE.pokemons,
    data: {}
}, action) {
    switch (action.type) {
      case types.GET:
        return {
            ...state,
            ...action.data
        }
      case types.DISMISS:
        return {
          page: PAGE.pokemons,
          data: {}
        }
      default:
        return state
    }
}

export default combineReducers({
    data: paginationReducer,
});
