import React, { lazy, useEffect, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { useDispatch, useSelector } from "react-redux";
import { doToast, getRandomColor } from "../../utilities/miscellaneous";
import { paginationOperations } from "../../application/pagination";
import { GET_POKEMON_DETAIL } from "../../graphql/get-pokemons";
import "./pokemonDetail.scss";
import { css } from "@emotion/css";
import { DataCorruptedError, NicknameAlreadyTakenError } from "../../utilities/errors";
import { pokemonOperations } from "../../application/pokemon";
import { savePokemon } from "../../utilities/pokemon";
const Popup = lazy(() => import('../../components/popup/Popup'));
const ReactLoading = lazy(() => import('react-loading'));
const PokeballButton = lazy(() => import('../../components/pokeballButton/PokeballButton'));

const PokemonDetailPage = ({pokemon={}}) => {
    const dispatch = useDispatch()
    const pokemonState = useSelector(state => state.Pokemon);
    const [popupVisible, setPopupVisible] = useState(false)
    const [nickname, setNickname] = useState("")
    const [errorPopup, setErrorPopup] = useState("")

    const DEFAULT_RESULT = {
        "pokemon": {
            "id": 0,
            "name": "",
            "sprites": {
              "front_default": ""
            },
            "moves": [
              {
                "move": {
                  "name": ""
                }
              }
            ],
            "types": [
              {
                "type": {
                  "name": ""
                }
              }
            ]
          }
    }

    const { loading, error, data = { DEFAULT_RESULT } } = useQuery(GET_POKEMON_DETAIL, {
        variables: { name: pokemon.name }
    })

    useEffect(() => {
        if(error) {
            doToast(error, "fail")
            dispatch(paginationOperations.dismiss())
        }
    }, [data, error])

    try {

        const saveThisPokemon = () => {
            try {
                let { url, name, image } = pokemon
                let { owned, inventory } = savePokemon({ url, name, image }, nickname, pokemonState.data.owned, pokemonState.data.inventory)
                dispatch(pokemonOperations.update({owned, inventory}))
                doToast(`${nickname} is now yours!`, "no-icon")
                setPopupVisible(false)
                setNickname("")
            } catch(e) {
                if(e instanceof NicknameAlreadyTakenError) {
                    setErrorPopup(e.message)
                } else {
                    console.log(e)
                    doToast(e.message)
                    if(e instanceof DataCorruptedError) {
                        dispatch(pokemonOperations.dismiss())
                    }
                    dispatch(paginationOperations.dismiss())
                }
            }
            
        }

        const catchPokemon = () => {
            try {
                if(Math.random() < 0.5) { // 50% chance
                    setPopupVisible(true)
                } else {
                    doToast(`Failed to catch ${pokemon.name}. Try Again!`, "fail")
                }
            } catch (e) {
                console.log(e)
                doToast(e.message)
                if(e instanceof DataCorruptedError) {
                    dispatch(pokemonOperations.dismiss())
                }
                dispatch(paginationOperations.dismiss())
            }
        }

        return <div className="pokemon-detail-outer-wrapper">
                    <Popup
                        visible={popupVisible}
                        onClose={() => {
                            setPopupVisible(false)
                            setNickname("")
                            setErrorPopup("")
                        }}
                        title={`You catch ${pokemon.name}! Name it.`}
                    >
                        <input
                            onChange={(e) => {
                                setNickname(e.target.value)
                                setErrorPopup("")
                            }}
                            onKeyUp={(e) => {
                                if (
                                    // using enter
                                    e.which === 13 &&
                                    // without holding shift
                                    !e.shiftKey &&
                                    nickname.trim() !== ""
                                ) {
                                    saveThisPokemon()
                                }
                            }}
                            className={errorPopup ? "error-input" : ""}
                        />
                        <p className="error-msg">{errorPopup}</p>
                    </Popup>
                {
                    loading ? 
                    <div className="loading">
                        <ReactLoading type="balls" color="var(--main-red)"/>
                    </div> 
                    : 
                    <div className="pokemon-detail-wrapper">
                        <div className="name">{data.pokemon.name}</div>
                        <img width={"70%"} height={"70%"} className="sprite" src={data.pokemon.sprites.front_default} alt={data.pokemon.name} />
                        <div className="pokemon-catch-button">
                            <PokeballButton 
                                className="pokemon-catch-button"
                                onClick={catchPokemon}
                            />
                        </div>
                        <div className="types">
                            {
                                data.pokemon.types.map((item, idx) => {
                                    return <div className={css`
                                        background-color: ${getRandomColor()};
                                        transform: scale(1);
                                        &:hover {
                                            transform: scale(1.1);
                                        }
                                    `}
                                    key={idx}
                                    >{item.type.name}</div>
                                })
                            }
                        </div>
                        <div className="pokemon-info-wrapper">
                            <div className="moves">
                                <div className="field">Moves</div>
                                <div className="value">
                                    <ul>
                                        {
                                            data.pokemon.moves.map((item, idx) => {
                                                return <li
                                                        // className={css`
                                                        //     top: 0;
                                                        //     &:hover {
                                                        //         top: -5px;
                                                        //     }
                                                        // `}
                                                        key={idx}
                                                    >
                                                        {item.move.name}
                                                    </li>
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                }
        </div>
    } catch(e) {
        doToast(e.message, "fail")
        dispatch(paginationOperations.dismiss())
        return ""
    }
}

export default PokemonDetailPage;
