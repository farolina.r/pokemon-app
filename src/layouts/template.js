import React, { Fragment } from "react";
import "react-toastify/dist/ReactToastify.css";
import { Fab, Action } from "react-tiny-fab";
import "react-tiny-fab/dist/styles.css";
import "./templateStyles.scss";
import pokeballClosed from "../assets/icons/pokemon-closed.png";
import { ToastContainer } from "react-toastify";
import pikachu from "../assets/icons/pikachu.png";
import snorlax from "../assets/icons/snorlax.png";
import eevee from "../assets/icons/eevee.png";
import { useDispatch } from "react-redux";
import { paginationOperations } from "../application/pagination";
import { PAGE } from "../application/pagination/pages";

const Template = ({ children }) => {
  const dispatch = useDispatch();
  try {
    return (
      <Fragment>
        <ToastContainer className="ToastContainer" />
        <div className="template-outer-wrapper">
          {children}
          <div className="menu-wrapper" id="menu-wrapper">
            <Fab
              icon={<img src={pokeballClosed} alt="menu" width={64} height={64} />}
              alwaysShowTitle={true}
            >
              <Action
                text="Pokemon List"
                onClick={() => dispatch(paginationOperations.get({ page: PAGE.pokemons }))}
              >
                <img src={pikachu} alt="Pokemon List" width={45} height={45} />
              </Action>
              <Action
                text="My Pokemon List"
                onClick={() => dispatch(paginationOperations.get({ page: PAGE.myPokemons }))}
              >
                <img src={snorlax} alt="Pokemon List" width={45} height={45} />
              </Action>
              <Action
                text="Info"
                onClick={() => dispatch(paginationOperations.get({ page: PAGE.info }))}
              >
                <img src={eevee} alt="Info" width={45} height={45} />
              </Action>
            </Fab>
          </div>
        </div>
      </Fragment>
    );
  } catch(e) {
    return ""
  }
};

export default Template;
