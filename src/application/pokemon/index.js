import reducer from "./reducers";

import * as pokemonOperations from "./operations";

export {
    pokemonOperations,
};

export default reducer;
