import React from "react";
import "./Info.scss";
import pokeballOpened from "../../assets/icons/pokemon-opened.png";
import pokeballClosed from "../../assets/icons/pokemon-closed.png";
import pokeballGlowing from "../../assets/icons/pokeball-glowing.png";
import pikachu from "../../assets/icons/pikachu.png";
import snorlax from "../../assets/icons/snorlax.png";
import eevee from "../../assets/icons/eevee.png";

const InfoPage = () => {
    return <div className="info-page-outer-wrapper">
        <div className="info-page-wrapper">
            <p className="title">CREDITS</p>
            <div className="icon-credit">
                <img src={pokeballOpened} alt="" width={64} height={64} />
                <div>
                    <a href="https://iconscout.com/icons/pokemon" target="_blank" rel="noreferrer">Pokemon Icon</a> by <a href="https://iconscout.com/contributors/pankti-bhalodia">Pankti Bhalodia</a> on <a href="https://iconscout.com">Iconscout</a>
                </div>
            </div>
            <div className="icon-credit">
                <img src={pokeballClosed} alt="" width={64} height={64} />
                <div>
                    <a href="https://iconscout.com/icons/pokemon" target="_blank" rel="noreferrer">Pokemon Icon</a> by <a href="https://iconscout.com/contributors/mcgandhi61" target="_blank" rel="noreferrer">Mohit Gandhi</a>
                </div>
            </div>
            <div className="icon-credits">
                <div>
                    <img src={pokeballGlowing} alt="" width={64} height={64} />
                    <img src={pikachu} alt="" width={64} height={64} />
                    <img src={snorlax} alt="" width={64} height={64} />
                    <img src={eevee} alt="" width={64} height={64} />
                </div>
                <div>
                    Icons made by <a href="https://www.flaticon.com/authors/webalys" title="Webalys" rel="noreferrer">Webalys</a> from <a href="https://www.flaticon.com/" title="Flaticon" rel="noreferrer">www.flaticon.com</a>
                </div>
            </div>
        </div>

        <div className="info-page-wrapper">
            <p className="title">API</p>
            <div>
                <a href="https://github.com/mazipan/graphql-pokeapi" target="_blank" rel="noreferrer">graphql-pokeapi</a>
            </div>
        </div>
    </div>
}

export default InfoPage;
