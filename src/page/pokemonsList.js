import React, { Fragment, lazy, useEffect, useRef, useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_POKEMONS } from "../graphql/get-pokemons";
import { doToast } from "../utilities/miscellaneous";
const ReactLoading = lazy(() => import('react-loading'));
const PokemonsList = lazy(() => import('../layouts/pokemonsList'));

function usePrevious(value) {
	const ref = useRef();

	useEffect(() => {
		ref.current = value;
	}, [value]); // Only re-run if value changes

	return ref.current;
}

const PokemonsListPage = () => {
    const GET_LIMIT = 50;
    const { loading, error, data = { pokemons: {} }, refetch } = useQuery(GET_POKEMONS, {
        variables: { limit: GET_LIMIT, offset: 0 }
    })
    let pageElement = useRef();
    const [pokemonsList, setPokemonsList] = useState([])

    useEffect(() => {
        const handleScroll = () => {
            if (pageElement.current) {
                if (
                    document.getElementById('load-more') &&
                    ((window.innerHeight + window.scrollY) >=
                    document.getElementById('load-more').offsetTop &&
                    pageElement.current.clientHeight)
                ) {
                    if(data && data.pokemons && data.pokemons.nextOffset) {
                        refetch({
                            limit: GET_LIMIT, offset: data.pokemons.nextOffset
                        })
                    }
                        
                }
            }
        };

        window.addEventListener('scroll', handleScroll, { passive: true });

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
	});

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [])

    let prevPokemons = usePrevious(data);
    useEffect(() => {
        try {
            if(data && data.pokemons && data.pokemons.results) {
                if(prevPokemons && data.pokemons.results !== prevPokemons.pokemons.results)  {
                    let tmp = []
                    tmp = pokemonsList.concat(data.pokemons.results)
                    setPokemonsList(tmp)
                } else {
                    // console.log(data.pokemons.results)
                    setPokemonsList(data.pokemons.results)
                }
            } else if(error) {
                doToast(error, "fail")
            }
        } catch(e) {
            console.log(e)
        }
    }, [data, error])

    return <Fragment>
                <PokemonsList
                    pokemonsList={pokemonsList}
                    loading={loading && pokemonsList.length === 0}
                    pageElement={pageElement}
                />
                {pokemonsList.length > 0 ? <div id="load-more" className="loading">
                    {loading ? <ReactLoading type="balls" color="var(--main-red)"/> : ""}
                </div> : ""}
            </Fragment>
}

export default PokemonsListPage;
