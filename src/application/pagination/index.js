import reducer from "./reducers";

import * as paginationOperations from "./operations";

export {
    paginationOperations,
};

export default reducer;
