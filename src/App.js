import './App.css';
import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import RoutesList from "./routes";
import configureStore from './state/store';
import { persistStore } from 'redux-persist';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

function App() {
  const store = configureStore(window.REDUX_INITIAL_DATA);
  const persistor = persistStore(store);
  
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Suspense fallback={'..'}>
          <Router>
            <Switch>
              {
                RoutesList.map(route => {
                  return <Route key={route.path} {...route} />
                })
              }
              <Redirect to="/" />
            </Switch>
          </Router>
        </Suspense>
      </PersistGate>
    </Provider>
  );
}

export default App;
