import { combineReducers } from "redux";
import * as types from "./types";

function pokemonReducer(state = {
    owned: {}, // owned pokemon data based on nickname
    inventory: {} // database of total pokemons owned for each pokemon based on pokemon's name
}, action) {
    switch (action.type) {
      case types.GET:
        return {
            ...state,
            ...action.data
        }
      case types.DISMISS:
        return {
          owned: {},
          inventory: {}
        }
      case types.UPDATE:
        return {
            ...state,
            ...action.data
        }
      default:
        return state
    }
}

export default combineReducers({
    data: pokemonReducer,
});
