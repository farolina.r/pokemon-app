import React, { Fragment, useEffect } from "react";
import "./Popup.scss";
import { IoClose } from "react-icons/io5"
import { zIndexLevel } from "../../utilities/constants";
import { toggleHideOverflowY } from "../../utilities/miscellaneous";

const Popup = ({
    children,
    title="",
    message="",
    visible=false,
    onClose=() => {}
}) => {

    useEffect(() => {
        toggleHideOverflowY(visible)
    })

  try {
    return visible ? <Fragment>
        <div 
            className="popup-overlay" 
            onClick={onClose}
            style={{
                zIndex: zIndexLevel.overlay
            }}
        ></div>
        <div
            className="popup-outer-wrapper"
            style={{
                zIndex: zIndexLevel.popup
            }}
        >
            <IoClose onClick={onClose} className="popup-close" />
            <p className="popup-title">{title}</p>
            <p className="popup-message">{message}</p>
            {children}
        </div>
        </Fragment> : ""
  } catch (e) {
    console.log(e);
    return "";
  }
};

export default Popup;
