import React, { Fragment, lazy } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./Pokemon.scss";
import { getOwnedPokemonTotal } from "../../utilities/pokemon";
import { paginationOperations } from "../../application/pagination";
import { PAGE } from "../../application/pagination/pages";
const TrashButton = lazy(() => import('../trashButton/TrashButton'));

const Pokemon = ({data = {}, owned=false, onRemove=()=>{}}) => {
    const dispatch = useDispatch()
    const pokemon = useSelector(state => state.Pokemon);

    try {

        const goToPokemonDetail = () => {
            dispatch(paginationOperations.get({ page: PAGE.pokemon, data: data }))
        }

        return <Fragment>
                    <div className="pokemon-outer-wrapper">
                        
                        <div className="pokemon-image-button-wrapper">
                            <img width={96} height={96} src={data.image} alt={data.name} className="pokemon-image" onClick={goToPokemonDetail} />
                            {
                                owned ?
                                <TrashButton 
                                    className="pokemon-catch-button"
                                    onClick={() => onRemove(data)}
                                />
                                :
                                ""
                            }
                        </div>
                        
                        <div className="name" onClick={goToPokemonDetail}>
                            {
                                owned ?
                                    <p className="nickname">{data.nickname} <span>THE</span></p>
                                    :
                                    ""
                            }
                            {data.name}
                        </div>

                        {
                            owned ?
                            ""
                            :
                            <div className="pokemon-owned-badge">
                                {getOwnedPokemonTotal(pokemon.data.inventory, data.name) + " OWNED"}
                            </div>
                        }
                    </div>
            </Fragment>
    } catch(e) {
        console.log(e)
        return ""
    }
}

export default Pokemon;
