import { createStore, applyMiddleware, combineReducers } from "redux";
// import expireReducer from 'redux-persist-expire';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import thunkMiddleware from "redux-thunk";
import { createLogger } from "./middlewares/logger";
import * as reducers from "../application";

export default function configureStore( initialState ) {
    const appReducer = combineReducers(reducers);
    const middleware = applyMiddleware(
        thunkMiddleware,
        createLogger( true ),
    )
    const rootPersistConfig = {
        key: 'root',
        storage: storage,
        whitelist: ['Pokemon', 'Pagination'],
        // transforms: [
        //     expireReducer('Pokemon', {
        //         expireSeconds: 3600,
        //     })
        // ]
    } 

    const rootReducer = (state, action) => {
        return appReducer(state, action);
    }

    const persist = persistReducer(rootPersistConfig, rootReducer);

    return createStore(
        persist,
        initialState,
        middleware,
    );
}
