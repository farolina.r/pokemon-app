import { DataCorruptedError, NicknameAlreadyTakenError, NoOwnedPokemonError } from "./errors";
import { isObjectEmpty } from "./miscellaneous";

export const savePokemon = (pokemon, nickname="", owned = {}, inventory = {}) => {
    try {
        if(owned[nickname]) {
            throw new NicknameAlreadyTakenError(nickname)
        } else {
            owned[nickname] = pokemon
        }
        
        let { name } =  owned[nickname];

        if(inventory[name]) {
            inventory[name] +=1
        } else {
            inventory[name] = 1
        }
        
        return { owned, inventory }
        
    } catch(e) {
        if(e instanceof NicknameAlreadyTakenError) {
            throw new NicknameAlreadyTakenError(e.nickname)
        }
        else throw new Error(e)
    }
}

export const getOwnedPokemonInfo = (owned = {}, inventory = {}) => {
    try {
        let ownedPokemons = [];
        if(isObjectEmpty(owned) || isObjectEmpty(inventory)) {
            throw new NoOwnedPokemonError()
        } else {
            for (let pokemon in owned) {
                if(inventory[owned[pokemon].name]) {
                    ownedPokemons.push({
                        nickname: pokemon,
                        qty: inventory[pokemon],
                        ...owned[pokemon]
                    })
                } else { // data corrupted. remove data
                    throw new DataCorruptedError()
                }
            }
        }
        return ownedPokemons
    } catch (e) {
        if(e instanceof NoOwnedPokemonError) {
            throw new NoOwnedPokemonError()
        } else if(e instanceof DataCorruptedError) {
            throw new DataCorruptedError()
        }
        else throw new Error(e)
    }
}

export const releasePokemon = (nickname="", owned = {}, inventory = {}) => {
    try {
        if(owned[nickname]) {
            let { name } =  owned[nickname];

            if(inventory[name]) {
                inventory[name] -= 1

                if(inventory[name] < 0) inventory[name] = 0
                delete owned[nickname];
            } else {
                throw new DataCorruptedError()
            }
        }

        return { owned, inventory }
        
    } catch(e) {
        if(e instanceof DataCorruptedError) {
            throw new DataCorruptedError()
        }
        else throw new Error(e)
    }
}

export const getOwnedPokemonTotal = (inventory = {}, name="") => {
    try {
        return (inventory[name] ? inventory[name] : 0)
    } catch (e) {
        return 0
    }
}
