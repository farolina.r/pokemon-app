import React, { useState } from "react";
import { IoTrashBinOutline, IoTrashBinSharp } from "react-icons/io5"

const TrashButton = ({
    className="",
    onClick=()=>{}
}) => {
    const [hovered, setHovered] = useState(false)
    return <div 
            className={`trash-button-outer-wrapper ${className}`}
            onMouseEnter={() => setHovered(true)}
            onMouseLeave={() => setHovered(false)}
            onClick={onClick}
        >
            {
                hovered ?
                    <IoTrashBinSharp color="var(--main-red)" />
                    :
                    <IoTrashBinOutline color="var(--main-red)" />
            }
    </div>
}

export default TrashButton;
