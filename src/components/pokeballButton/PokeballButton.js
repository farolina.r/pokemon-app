import React from "react";
// import pokeball from "../../assets/icons/pokeball.png";
import pokeballGlow from "../../assets/icons/pokeball-glowing.png";
import "./PokeballButton.scss"
// import { css } from "@emotion/css";

const PokeballButton = ({
    onClick=()=>{},
    className=""
}) => {
    try {
        return <div className={`pokeball-button-outer-wrapper ${className}`} onClick={onClick}>
            <img
                src={pokeballGlow}
                // className={css`
                //     content: url(${pokeball});
                //     &:hover {
                //         content: url(${pokeballGlow})
                //     }
                //     &:active {
                //         &:focus {
                //             content: url(${pokeballGlow})
                //         }
                //     }
                // `}
                alt=""
                width={24}
                height={24}
            />
            <div className="text">
                <p>CATCH!</p>
            </div>
        </div>
    } catch(e) {
        console.log(e)
        return ""
    }
}

export default PokeballButton;
