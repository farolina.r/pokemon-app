import React, { Fragment, lazy, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { pokemonOperations } from "../application/pokemon";
import { paginationOperations } from "../application/pagination";
import { DataCorruptedError } from "../utilities/errors";
import { doToast } from "../utilities/miscellaneous";
import { getOwnedPokemonInfo, releasePokemon } from "../utilities/pokemon";
const PokemonsList = lazy(() => import('../layouts/pokemonsList'));

const MyPokemonsListPage = () => {
    const dispatch = useDispatch()
    const [pokemonsList, setPokemonsList] = useState([])
    const pokemon = useSelector(state => state.Pokemon);

    const removePokemon = (item) => {
        try {
            let { owned, inventory } = releasePokemon(item.nickname, pokemon.data.owned, pokemon.data.inventory);
            dispatch(pokemonOperations.update({ owned, inventory }))
            doToast(`You've released ${item.nickname} the ${item.name}!`)
            let ownedList = getOwnedPokemonInfo(owned, inventory)
            setPokemonsList(ownedList)
        } catch(e) {
            doToast(e.message)
            if(e instanceof DataCorruptedError) {
                dispatch(pokemonOperations.dismiss())
            }
            dispatch(paginationOperations.dismiss())
        }
    }

    useEffect(() => {
        window.scrollTo(0, 0);
        try {
            setPokemonsList(getOwnedPokemonInfo(pokemon.data.owned, pokemon.data.inventory))
        } catch(e) {
            doToast(e.message)
            if(e instanceof DataCorruptedError) {
                dispatch(pokemonOperations.dismiss())
            }
            dispatch(paginationOperations.dismiss())
        }
    }, [])

    return <Fragment>
        <PokemonsList
            pokemonsList={pokemonsList}
            owned={true}
            onRemove={removePokemon}
            // loading={pokemonsList.length === 0}
        />
    </Fragment>
}

export default MyPokemonsListPage;
