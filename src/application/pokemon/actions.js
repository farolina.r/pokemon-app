import * as types from "./types";

export const update = ( data ) =>({
    type: types.UPDATE,
    data
});

export const get = ( data ) =>({
    type: types.GET,
    data
});

export const dismiss = () =>({
    type: types.DISMISS
});