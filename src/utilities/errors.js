export class ValidationError extends Error {
  constructor(message) {
    super(message);
    this.name = "ValidationError";
  }
}

export class NoOwnedPokemonError extends Error {
  constructor() {
    super("You don't own any pokemons. Catch them on Pokemons List");
    this.name = "NoOwnedPokemonError";
  }
}

export class DataCorruptedError extends Error {
  constructor() {
    super("Pokemon data is corrupted.");
    this.name = "DataCorruptedError";
  }
}

export class NicknameAlreadyTakenError extends ValidationError {
  constructor(nickname) {
    super("Nickname's already taken: " + nickname);
    this.name = "NicknameAlreadyTakenError";
    this.nickname = nickname;

    // Object.setPrototypeOf(this, NicknameAlreadyTakenError.prototype);
  }
}
