export const PAGE = {
    pokemons: "POKEMONS_LIST",
    myPokemons: "MY_POKEMONS",
    info: "WEB_INFO",
    pokemon: "POKEMON_DETAIL"
}
