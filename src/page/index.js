import React, { lazy } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { PAGE } from "../application/pagination/pages";
import { useSelector } from "react-redux";
import { Helmet } from "react-helmet";
const Template = lazy(() => import('../layouts/template'));
const PokemonsListPage = lazy(() => import('./pokemonsList'));
const PokemonDetailPage = lazy(() => import('./pokemonDetail/pokemonDetail'));
const MyPokemonsListPage = lazy(() => import('./myPokemonsList'));
const InfoPage = lazy(() => import('./info/Info'));

const PageContainer = () => {
    const client = new ApolloClient({
        uri: "https://graphql-pokeapi.vercel.app/api/graphql"
    });
    const pagination = useSelector(state => state.Pagination);

    const renderPage = () => {
        switch (pagination.data.page) {
            case PAGE.pokemons:
                return <PokemonsListPage />
            case PAGE.myPokemons:
                return <MyPokemonsListPage />
            case PAGE.pokemon:
                return <PokemonDetailPage pokemon={pagination.data.data} />
            case PAGE.info:
                return <InfoPage />
            default:
                return <PokemonsListPage />
        }
    }

    const getHeaderTitle = () => {
        switch (pagination.data.page) {
            case PAGE.pokemons:
                return {title: "Pokemon App", page: "Pokemons"}
            case PAGE.myPokemons:
                return {title: "My Pokemons - Pokemon App", page: "My Pokemons"}
            case PAGE.pokemon:
                return {title: `${pagination.data.data ? String(pagination.data.data.name).toUpperCase() : "Pokemon Detail"} - Pokemon App`, page: ""}
            case PAGE.info:
                return {title: "Info - Pokemon App", page: ""}
            default:
                return {title: "Pokemon App", page: "Pokemons"}
        }
    }

    return <ApolloProvider client={client}>
        <Helmet>
            <title>
                { getHeaderTitle().title }
            </title>
        </Helmet>
        <Template>
            <h3>
                { getHeaderTitle().page }
            </h3>
            { renderPage() }
        </Template>
    </ApolloProvider>
}

export default PageContainer;
