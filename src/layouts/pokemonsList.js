import React, { Fragment, lazy } from "react";
import "./pokemonsList.scss";
const Pokemon = lazy(() => import('../components/pokemon/Pokemon'));
const ReactLoading = lazy(() => import('react-loading'));

const PokemonsList = ({
    loading=false,
    pokemonsList=[],
    pageElement=null,
    owned=false,
    onRemove=()=>{}
}) => {
    return <Fragment>
        <div className="pokemons-list-outer-wrapper" ref={pageElement}>
            {pokemonsList.map((item, idx) => <Pokemon data={item} key={idx} owned={owned} onRemove={onRemove} />)}
        </div>
        {
            loading ?
            <div className="loading">
                <ReactLoading type="balls" color="var(--main-red)"/>
            </div>
            : ""
        }
    </Fragment>
}

export default PokemonsList;
