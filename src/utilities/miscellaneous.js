import { toast } from "react-toastify";

export const doToast = (text = "", _className = null, position = null) => {
  if (text) {
    const config = {
      autoClose: 2000,
      position: position ? position : "bottom-center",
      className:
        _className === null ? "ToastContent" : "ToastContent-" + _className,
      hideProgressBar: true,
    };

    toast(text, config);
  }
};

export const toggleHideOverflowY = (toggle) => {
  let target = document.querySelector("html");

  if (toggle) {
    target.classList.add("hideoverflow-y");
  } else {
    target.classList.remove("hideoverflow-y");
  }
};

export const isObjectEmpty = (obj) => {
  return obj && Object.keys(obj).length === 0 && Object.getPrototypeOf(obj) === Object.prototype
}

export const getRandomColor = () => {
  // pokemon color scheme
  const colors = [
    "#FF0000", // red
    "#CC0000", // Boston University Red
    "#3B4CCA", // cerulean blue
    "#3B4CCA", // golden yellow
    "#B3A125", // gold foil
  ]

  return colors[Math.floor(Math.random()*colors.length)]
}
